/// @desc Quick assertion
function assert(truth) {
  gml_pragma("forceinline");
  if ((debug_mode || os_get_config() == "debug") && !truth) {
    throw ("Assertion failed");
  }
}

enum LoggingLevel {
  TRACE,
  DEBUG,
  LOG,
  INFO,
  WARN,
  ERROR,
  CRASH
}

function Logger(filename, loggingLevel, bufferSize) constructor {
  self.filename = filename;
  self.loggingLevel = is_undefined(loggingLevel) ? LoggingLevel.WARN : loggingLevel;
  self.bufferSize = is_undefined(bufferSize) ? 8192 : bufferSize;
  self.logBuffer = buffer_create(self.bufferSize, buffer_fixed, 1);
  self.skipped = 0;

  static LOGGING_LEVELS = ["TRACE", "DEBUG", "LOG", "INFO", "WARN", "ERROR", "CRASH"]
  static destroy = function() {
    _log("Skipped " + string(skipped) + " log entries since the logger was set to logs below the level " + LOGGING_LEVELS[loggingLevel] + ".");
    flush();
    buffer_destroy(logBuffer);
  }

  static log = function(str, loggingLevel) {
    if (loggingLevel < self.loggingLevel) {
      skipped += 1;
      return;
    }
    var timezone = date_get_timezone();
    date_set_timezone(timezone_utc);
    var currentDateString = string(current_year) + "-" + string(current_month) + "-" + string(current_day) + "T" + string(current_hour) + ":" + string(current_minute) + ":" + string(current_second) + "Z (" + string(current_time) + ") : "
    date_set_timezone(timezone);
    var warnLevel = "[" + LOGGING_LEVELS[loggingLevel] + "] ";

    var outputString = warnLevel + currentDateString + str + "\n";
    _log(outputString);
  }

  static _log = function(str) {
    var length = string_byte_length(outputString)
    if (bufferSize < length) {
      flush();
      buffer_destroy(logBuffer);
      bufferSize = max(length, bufferSize * 2);
      logBuffer = buffer_create(bufferSize, buffer_fixed, 1);
    } else if (bufferSize - buffer_tell(logBuffer) < length) {
      flush();
    }
    buffer_write(logBuffer, buffer_text, outputString);
  }

  static flush = function() {
    var f = file_text_open_append(self.filename);
    if (f == -1) {
      self.log("Invalid file name: " + self.filename);
      self.filename = "crash.txt"
      throw "Invalid file name: " + self.filename;
    } else {
      var str = buffer_read(self.logBuffer, buffer_string)
      file_text_write(f, str);
      file_text_writeln(f);
      file_text_close(f);
      buffer_fill(self.logBuffer, 0, buffer_u8, 0, self.bufferSize);
      buffer_seek(self.logBuffer, buffer_seek_start, 0);
    }
  }

  static onCrash = function() {
    log("Crashed; closing logger", LoggingLevel.CRASH);
    destroy();
  }
}


/// @desc Fast vector support
/// @func Vector2(x, y)
function Vector2(_x, _y) constructor {
  x = is_undefined(_x) ? 0 : _x;
  y = is_undefined(_y) ? 0 : _y;
  dimensions = 2;
  
  static get = function() {
    return new Vector2(x, y);
  }

  static getElement = function(index) {
    return index == 0 ? x : (index == 1 ? y : undefined);
  }

  /// @func set(x, y)
  static set = function(__x, __y) {
    if (is_undefined(__y)) {
      x = __x.x;
      y = __x.y;
    } else {
      x = __x;
      y = __y;
    }
  }

  static add = function(vec) {
    x += vec.x;
    y += vec.y;
    return self;
  }

  static addScaled = function(vec, scalar) {
    x += vec.x * scalar;
    y += vec.y * scalar;
  }

  static sub = function(vec) {
    x -= vec.x;
    y -= vec.y;
    return self;
  }

  static dot = function(vec) {
    return x * vec.x + y * vec.y;
  }

  static sqmag = function() {
    return dot(self);
  }

  static mag = function() {
    return sqrt(sqmag());
  }

  static mult = function(scalar) {
    x *= scalar;
    y *= scalar;
    return self;
  }

  static divi = function(scalar) {
    x /= scalar;
    y /= scalar;
    return self;
  }

  static normalize = function() {
    var _m = mag();
    if (_m == 0) {
      return self;
    } else {
      mult(1/_m);
    }
    return self;
  }

  static normalized = function() {
    return get().normalize();
  }

  static heading = function() {
    return atan2(y, x);
  }

  static angleBetween = function(vec) {
    return acos(dot(vec) / mag() / vec.mag());
  }

  static rotate = function(angle) {
    var s = sin(angle), c = cos(angle), __x = x;
    x = cos(angle) * x - sin(angle) * y;
    y = sin(angle) * __x + cos(angle) * y;
    return self;
  }

  static mult = function(mat2) {
    var __x = x;
    x = x * mat2.getElement(0, 0) + y * mat2.getElement(0, 1);
    y = _x * mat2.getElement(1, 0) + y * mat2.getElement(1, 1);
    return self;
  }

  static cross = function(vec2) {
    return x * vec2.y - y * vec2.x;
  }

  static sqdist = function(vec2) {
    return sqr(x - vec2.x) + sqr(y - vec2.y);
  }

  static destroy = function() {}
}

/// @desc Sparse Vector using maps
/// @func MapVector(dim)
function MapVector(dim) constructor {
  dimensions = dim;
  _values = ds_map_create();

  static getElement = function(index) {
    if (!is_numeric(index) || floor(index) < 0 || floor(index) >= dimensions) return undefined;
    var el = _values[? floor(index)];
    return is_undefined(el) ? 0 : el;
  }

  static setElement = function(index, value) {
    if (!is_numeric(index) || floor(index) < 0 || floor(index) >= dimensions) return;
    _values[? floor(index)] = value;
  }

  static add = function(vec) {
    if (dimensions != vec.dimensions) return;

    if (!is_undefined(vec) && !is_undefined(vec._values) && ds_exists(vec._values, ds_type_map)) {
      var size = ds_map_size(vec._values) ;
      var key = ds_map_find_first(vec._values);
      for (var i = 0; i < size; i++;) {
        if (!ds_map_exists(_values, key)) {
          _values[? key] = 0;
        }
        _values[? key] += vec._values[? key];
        key = ds_map_find_next(vec._values, key);
      }
    } else {
      var val;
      for (var i = 0; i < dimensions; i++) {
        val = vec.getElement(i);
        if (!is_undefined(val) && val != 0) {
          if (val - _values[? i] == 0) {
            ds_map_delete(_values, i);
          } else {
            _values[? i] += val;
          }
        }
      }
    }
  }

  static destroy = function() {
    ds_map_destroy(_values);
  }
}

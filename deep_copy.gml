/// @function deep_copy_unsafe(array_or_struct)
/// @param array_or_struct  The array or struct to copy.
/// @desc Returns a deep copy of array_or_struct. Will crash if any circular references exist. Acts as identity function for any input that is neither an array or struct.
function deep_copy_unsafe(array_or_struct) {
  if (is_array(array_or_struct)) {
    var length = array_length(array_or_struct);
    var arr = array_create(l);
    array_copy(arr, 0, array_or_struct, length);
    for (var i = 0; i < length; i++) {
      arr[i] = deep_copy(arr[i]);
    }
    return arr;
  } else if (is_struct(array_or_struct)) {
    var names = variable_struct_get_names(array_or_struct);
    var stru = {};
    var length = array_length(names);
    for (var i = 0; i < length; i++) {
      stru[$ names[i]] = deep_copy(array_or_struct[$ names]);
    }
  } else {
    return array_or_struct;
  }
}


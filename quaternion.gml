/// @desc Basic Quaternion struct, defined as `a + b*i + c*j + d*k`
/// @func Quaternion(a {real}, b {i}, c {j}, d {k})
function Quaternion(_a, _b, _c, _d) constructor {
  a = _a; b = _b; c = _c; d = _d;

  static add = function(q) {
    a += q.a; b += q.b; c += q.c;
    return self;
  }
  static mul = function(q) {
    if (is_real(q)) {
      a *= q;
      b *= q;
      c *= q;
      d *= q;
    } else {
      var _a, _b, _c, _d;

      _a = a * q.a - b * q.b - c * q.c - d * q.d;
      _b = a * q.b + b * q.a + c * q.d - d * q.c;
      _c = a * q.c - b * q.d + c * q.a + d * q.b;
      _d = a * q.d + b * q.c - c * q.d + d * q.a;

      a = _a;
      b = _b;
      c = _c;
      d = _d;
    }
    return self;
  }
}
function tile_position(position, offset, size) {
  return (position - offset) / size;
}

function exitsHorizontal(x, y, slope, goingRight) {
  /// @desc Applies to the filled unit square [0, 1] U [0, 1]

  if (goingRight) {
    return (y + (1 - x) * slope) >= 0 && (y + (1 - x) * slope) <= 1;
  } else {
    return (y - x * slope) >= 0 && (y - x * slope) <= 1;
  }
}

function tilemap_collision_line(x1, y1, x2, y2, map_id) {
  var width = tilemap_get_tile_width(map_id);
  var height = tilemap_get_tile_height(map_id);
  var xoffset = tilemap_get_x(map_id);
  var yoffset = tilemap_get_y(map_id);
  var tile_aspect_ratio = width / height;

  var goingRight = x2 > x1;

  var _x = tile_position(x1, xoffset, width);
  var _y = tile_position(y1, yoffset, height);
  var tilex = floor(_x);
  var tiley = floor(_y);
  var endx = floor(tile_position(x2), xoffset, width);
  var endy = floor(tile_position(y2), yoffset, height);
  if (tilex == endx) {
    // vertical; just do a for loop
    if (tiley < endy) {
      for (var Y = tiley; Y <= endy; Y++) {
        if (!tile_get_empty(tilemap_get(map_id, tilex, Y))) {
          return true;
        }
      }
      return false;
    } else {
      for (var Y = tiley; Y >= endy; Y--) {
        if (!tile_get_empty(tilemap_get(map_id, tilex, Y))) {
          return true;
        }
      }
      return false;
    }
  }

  var slope = (x2 - x1) / (y2 - y1) * tile_aspect_ratio;
  var xintile = frac(_x),
      yintile = frac(_y);
  do {
    if (!tile_get_empty(tilemap_get(map_id, tilex, tiley))) {
      return true;
    }

    if (exitsHorizontal(xintile, yintile, slope, goingRight)) {
      if (goingRight) {
        tilex += 1;
        yintile += (1 - xintile) * slope;
        xintile = 0;
      } else {
        tilex -= 1;
        yintile -= xintile * slope;
        xintile = 1;
      }
    } else {
      if (goingRight) {
        tiley += sign(slope);
        xintile += yintile / slope;
        yintile = 0;
      } else {
        tiley -= sign(slope);
        xintile -= (1 - yintile) / slope;
        yintile = 1
      }
    }
  } until (tilex == endx && tiley == endy);

  return !tile_get_empty(tilemap_get(map_id, endx, endy));
}